als-ans-role-repository
=======================

This Ansible role enables additional repositories required for system support and installation.

For CentOS:
- CentOS 7 (mirror)
- EPEL (mirror)

Requirements
------------

- ansible >= 2.7
- molecule >= 2.14

Role Variables
--------------

```yaml
repository_centos_version: 7.6.1810
repository_installroot: "/"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: als-ans-role-repository
```

License
-------

BSD 2-clause
