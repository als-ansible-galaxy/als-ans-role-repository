import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-repository-debian')


def test_zabbix_debian_install(host):
    cmd = host.run('apt-cache policy')
    assert 'zabbix-mirror' in cmd.stdout
